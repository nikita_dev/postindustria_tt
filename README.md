Used libraries and technologies:
===============================
jQuery 3.1.1
MySQL 5.7.11
PHP 7.0.9
Symfony framework 3.1.6

Sources for the 1st task are located in: /src/AppBundle/Controller/Init.php ; /src/AppBundle/Entity/Test.php ; /src/AppBundle/Repository/TestRepository.php

The answer for the 2nd task are shown bellow:

About the query. No need to select fields from table 'link', they are in tables 'data' and 'info'. Also it would be better to add the 'limit' operator to the query, in order to avoid major impact on performance.

Optimized query will look like this:

SELECT data.*, info.*
FROM data,link,info 
WHERE link.info_id = info.id 
  AND link.data_id = data.id
LIMIT 1000;

About DB tables. It would be better to add indexes on each field in the 'link' table, to improve performance of the joining tables 'data' and 'info' like in previous query. it can be done by the next query.

ALTER TABLE `link` 
  ADD INDEX(`data_id`),
  ADD INDEX(`info_id`);

Sources for the 3rd task are located in: /src/AppBundle/Controller/RegExp.php ; /src/AppBundle/Service/FileFinder.php

Sources for the 4th task are located in: /app/Resources/views/test/jquery.html.twig ; /web/js/test.js

Configuration of the web server from file 'httpd-vhosts' are shown bellow:

<VirtualHost *:80>
    ServerName symfony.loc
    ServerAlias www.symfony.loc

    DocumentRoot D:\WebServer\htdocs\postindustria\web
    <Directory D:\WebServer\htdocs\postindustria\web>
        AllowOverride None
        Order Allow,Deny
        Allow from All

        <IfModule mod_rewrite.c>
            Options -MultiViews
            RewriteEngine On
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^(.*)$ app_dev.php [QSA,L]
        </IfModule>
    </Directory>

    # uncomment the following lines if you install assets as symlinks
    # or run into problems when compiling LESS/Sass/CoffeScript assets
    # <Directory /var/www/project>
    #     Options FollowSymlinks
    # </Directory>

    # optionally disable the RewriteEngine for the asset directories
    # which will allow apache to simply reply with a 404 when files are
    # not found instead of passing the request into the full symfony stack
    <Directory D:\WebServer\htdocs\symfony-lessons>
        <IfModule mod_rewrite.c>
            RewriteEngine Off
        </IfModule>
    </Directory>
    ErrorLog D:\WebServer\apache\logs\postindustria.log
    CustomLog D:\WebServer\apache\logs\postindustria.log combined
</VirtualHost>

Before run the project please run the next commands from project root folder: 'composer install', 'bower install'.