var buttonsContainer = $('#test-container'),
    buttonTmpl = $('<button class="test"></button>');

for (var i = 1; i < 4; i++) {
    buttonsContainer.append(buttonTmpl.clone().html(i));
}

buttonsContainer.on('click', 'button', function () {
    buttonsContainer.children().first().detach().appendTo(buttonsContainer);
});