<?php
	
	namespace AppBundle\Repository;
	
	use AppBundle\Entity\Test;
	use Doctrine\ORM\EntityRepository;
	
	class TestRepository extends EntityRepository
	{
		/**
		 * creates test table
		 * @return bool
		 */
		public function createTable() : bool
		{
			$stmt = $this
				->getEntityManager()
				->getConnection()
				->prepare(
					'CREATE TABLE IF NOT EXISTS test (
					test_id INT AUTO_INCREMENT NOT NULL, 
					script_name VARCHAR(25) NOT NULL, 
					start_time DATETIME NOT NULL, 
					end_time DATETIME NOT NULL, 
					result ENUM(\'normal\', \'illegal\', \'failed\', \'success\') NOT NULL, 
					PRIMARY KEY(test_id)) 
					DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;'
				);
			
			return $stmt->execute();
		}
		
		/**
		 * fills the test table by random data
		 * @param int $rows
		 */
		public function fillTable(int $rows = 5)
		{
			$em = $this->getEntityManager();
			
			$date = new \DateTime();
			
			$testReflection = new \ReflectionClass(Test::class);
			
			//get all possible result values
			$resultVariants = $testReflection->getConstants();
			
			for ($i = 0; $i < $rows; $i++) {
				$test = new Test();
				$test->setScriptName(uniqid());
				
				$date->modify('+1 hour');
				
				$test->setStartTime(clone $date);
				
				$date->modify('+1 hour');
				
				$test->setEndTime(clone $date);
				
				$test->setResult(
					$resultVariants[
						array_rand($resultVariants)
					]
				);
				
				$em->persist($test);
			}
			
			$em->flush();
		}
	}