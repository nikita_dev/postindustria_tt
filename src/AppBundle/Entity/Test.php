<?php
	
	namespace AppBundle\Entity;
	
	use Doctrine\ORM\Mapping as ORM;
	
	/**
	 * @ORM\Entity(repositoryClass="AppBundle\Repository\TestRepository")
	 * @ORM\Table(name="test")
	 */
	class Test
	{
		const RESULT_NORMAL = 'normal';
		
		const RESULT_ILLEGAL = 'illegal';
		
		const RESULT_FAILED = 'failed';
		
		const RESULT_SUCCESS = 'success';
		
		/**
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 * @ORM\Column(name="test_id", type="integer")
		 */
		private $id;
		
		/**
		 * @ORM\Column(name="script_name", type="string", length=25)
		 */
		private $scriptName;
		
		/**
		 * @ORM\Column(name="start_time", type="datetime")
		 */
		private $startTime;
		
		/**
		 * @ORM\Column(name="end_time", type="datetime")
		 */
		private $endTime;
		
		/**
		 * @ORM\Column(type="string", columnDefinition="ENUM('normal', 'illegal', 'failed', 'success')")
		 */
		private $result;
		
		/**
		 * @return int
		 */
		public function getId() : int
		{
			return $this->id;
		}
		
		/**
		 * @param int $id
		 * @return Test
		 */
		public function setId(int $id) : self
		{
			$this->id = $id;
			return $this;
		}
		
		/**
		 * @return mixed
		 */
		public function getScriptName() : string
		{
			return $this->scriptName;
		}
		
		/**
		 * @param string $scriptName
		 * @return Test
		 */
		public function setScriptName(string $scriptName) : self
		{
			$this->scriptName = $scriptName;
			return $this;
		}
		
		/**
		 * @return \DateTime
		 */
		public function getStartTime() : \DateTime
		{
			return $this->startTime;
		}
		
		/**
		 * @param \DateTime $startTime
		 * @return Test
		 */
		public function setStartTime(\DateTime $startTime) : self
		{
			$this->startTime = $startTime;
			return $this;
		}
		
		/**
		 * @return \DateTime
		 */
		public function getEndTime() : \DateTime
		{
			return $this->endTime;
		}
		
		/**
		 * @param \DateTime $endTime
		 * @return Test
		 */
		public function setEndTime(\DateTime $endTime) : self
		{
			$this->endTime = $endTime;
			return $this;
		}
		
		/**
		 * @return string
		 */
		public function getResult() : string
		{
			return $this->result;
		}
		
		/**
		 * @param string $result
		 * @return Test
		 */
		public function setResult(string $result) : self
		{
			$this->result = $result;
			return $this;
		}
	}