<?php

	namespace AppBundle\Controller;
	
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	
	class RegExp extends Controller
	{
		/**
		 * @Route("/find")
		 */
		public function findFiles()
		{
			$files = $this->get('app.file_finder')->find(
				dirname($this->get('kernel')->getRootDir()) . '\datafiles'
			);
			
			foreach ($files as $file)
			{
				dump($file->getFilename());
			}
			die;
		}
	}