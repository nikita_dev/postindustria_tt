<?php
	
	namespace AppBundle\Controller;
	
	use AppBundle\Entity\Test;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Component\HttpFoundation\Response;
	
	final class Init extends Controller
	{
		/**
		 * @return bool
		 */
		private function create()
		{
			return $this->getDoctrine()->getRepository('AppBundle:Test')->createTable();
		}
		
		private function fill()
		{
			$this->getDoctrine()->getRepository('AppBundle:Test')->fillTable();
		}
		
		/**
		 * @Route("/")
		 * @return \AppBundle\Entity\Test[]|array
		 */
		public function getAction()
		{
			$list = $this->getDoctrine()->getRepository('AppBundle:Test')->findBy(
				['result' =>
					[Test::RESULT_NORMAL, Test::RESULT_SUCCESS]
				]
			);
			
			return $this->render('test/jquery.html.twig', [
				'list' => $list
			]);
		}
	}