<?php
	
	namespace AppBundle\Service;
	
	use Symfony\Component\Finder\Finder;
	
	class FileFinder
	{
		/**
		 * finds alphanumeric filenames with TXT extension in /datafiles folder, sorted by filename
		 * @param string $path
		 * @return Finder|\Symfony\Component\Finder\SplFileInfo[]
		 */
		public function find(string $path) : Finder
		{
			$finder = new Finder();
			return $finder->files()->in($path)->name('/[a-z0-9]+\.txt/i')->sortByName();
		}
	}